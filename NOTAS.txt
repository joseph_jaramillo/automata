## Crear un nuevo repositorio:
git clone git@gitlab.com:joseph_jaramillo/automata.git
cd automata
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

## Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:joseph_jaramillo/automata.git
git add .
git commit -m "Initial commit"
git push -u origin master

## Desactivar advertencias LF CRLF:
git config --global core.autocrlf true